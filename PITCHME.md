<!-- $size: 16:9 -->
<!-- page_number: true -->
<!-- footer: Roland Stumpner GPLv3 -->



## Portainer.io
* Microservices and Container
* Version 20180320

---
## Portainer.io vLAB Übersicht
* Voraussetzungen vLAB
* Portainer Einführung
* Portainer First Steps
* Portainer Aufgaben Level 1

---
## Portainer.io Voraussetzungen vLAB (on-prem)
* 2 x CPU Core
* 2 GB RAM (minimal)
* 10 GB Space
* Installation of Virtualbox (https://www.virtualbox.org/)
---
## The vLAB Environment Level 1
* portainervm.local
  * (Ubuntu 16.04 / docker-ce / portainer.io / Port 8082 / 8083/ 9000 )

---
## Einführung Portainer
Ein Leichtgewichtiges UI für Containermanagement
from Localhost to Container Clusters (Kubernetes / Docker Swarm / Mesos)


---
## Setup the Virtual Lab Environment
### (Vagrant)
* git clone https://www.gitlab.com/rstumpner/sam-vlab-devops-portainer
* cd sam-vlab-devops-docker/vlab/vagrant-virtualbox/
* vagrant up
* vagrant status
* vagrant ssh

---
## Installation of Portainer.io
(manual)

Installation Docker:

* Ubuntu 16.04 LTS Basis
* Install the new Docker-CE PGP Key
```
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
```
* Install the Docker-CE Repository
```
sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"
```
* Update Repositorys
```
sudo apt-get update
```
* Install Docker-CE
```
sudo apt-get install docker-ce
```
* Enable Docker at Startup
```
sudo systemctl enable docker
```
---
## Starte Portainer Container
```
sudo docker run -d -p 9000:9000 -v
"/var/run/docker.sock:/var/run/docker.sock" portainer/portainer
```
---
## Konguration über Webinterface
* admin / sam123456
* Manage localhost
---
## Konguration über Webinterface
![](_images/portainer-connect.png)
---
## Manual Installation of Portainer
### (Alterntive)
* Install Docker
```bash
wget -qO- https://get.docker.com/ | sh
```
* Starte Portainer
```bash
sudo docker run -d -p 9000:9000 -v
"/var/run/docker.sock:/var/run/docker.sock" portainer/portainer
```
* Konguration über Webinterface
  * admin / sam123456
  * Manage localhost


---
## Basiskonzept Microservices
* Entwicklerseite:
  * Austauschbarkeit des Services (2 Wochen)
* Infrastrukturseite:
* schnelle Deployment Zyklen
* Cloud Aware bis Native Anwendungen
* Leichgewichtig (One Service per Container) oder as Service

---
## First Steps in Portainer
Install a Database Container from Dockerhub

![mariadb](_images/portainer-mariadb.png)
---
## First Steps in Portainer
Debug a Database Container

![mariadb](_images/portainer-mariadb-debug.png)

---
## First Steps in Portainer
Install a Web Container from Templates

![mariadb](_images/portainer-mariadb.png)

---
## Aufgaben Portainer.io Level 1
#### Level 1

Installiere einen Web Stack mit einer Applikation nach dem Microservice Konzepten

##### Beispiel:
* Loadbalancer 
  * NGNIX (https://github.com/jwilder/nginx-proxy)
  * Traffic-Proxy (https://traefik.io/)
* Kanboard (https://kanboard.org/)
* Apache / PHP
* MariaDB


---
## Anhang Portainer.io Level 1

![Portainer Arichiteḱtur](_images/sam-vlab-portainer-rolling.png)

---
## Aufgaben Portainer.io Level 1

![Portainer Arichiteḱtur](_images/sam-vlab-portainer-skizze.png)

---
# Portainer.io Cheat Sheet

* sudo docker run -d -p 9000:9000 -v
"/var/run/docker.sock:/var/run/docker.sock" portainer/portainer
* http://localhost:9000 (http://localhost:9000)
---
# Links:
* Portainer.io
  http://portainer.io/
* NGNIX 
  https://github.com/jwilder/nginx-proxy
* Traefik-Proxy 
  https://traefik.io/

---
# Notizen
